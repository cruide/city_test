<?php

class CheckIP
{
    protected $client_ip;
    protected $response;

    /**
    * Constructor of class
    */
    public function __construct()
    {
        global $_GET, $_POST;

        /**
        * Get IP form stdin ($_GET, $_POST)
        */
        if( !empty($_POST['ip']) ) {
            $ip = $_POST['ip'];

        } else if( !empty($_GET['ip']) ) {
            $ip = $_GET['ip'];

        } else {
            $ip = $this->getIpAddress();
        }

        /**
        * Check for valid IP address
        */
        if( filter_var($ip, FILTER_VALIDATE_IP, array(FILTER_FLAG_IPV4)) ) {
            $this->client_ip = $ip;

        } else {
            die('Incorrect IP address...');
        }

        /**
        * Get json data from remote API for IP
        */
        $json_api_response = file_get_contents('http://api.sypexgeo.net/json/' . $this->client_ip);

        if( !empty($json_api_response) ) {
            $this->response = json_decode( $json_api_response );
        }
    }

    /**
    * Return client IP address
    *
    */
    public function getIpAddress()
    {
        global $_SERVER;

        if( !empty($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['HTTP_CLIENT_IP']) ) {
            $ipaddr = $_SERVER['HTTP_CLIENT_IP'];

        } else if( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
            $ipaddr = $_SERVER['HTTP_CLIENT_IP'];

        } else if( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
            $ipaddr = $_SERVER['HTTP_X_FORWARDED_FOR'];

        } else {
            $ipaddr = $_SERVER['REMOTE_ADDR'];
        }

        if( $ipaddr === false ) {
            return '0.0.0.0';
        }

        if( strstr($ipaddr, ',') ) {
            $x = explode(',', $ipaddr);
            $ipaddr = end( $x );
        }

        if( filter_var($ipaddr, FILTER_VALIDATE_IP) === false ) {
            $ipaddr = '0.0.0.0';
        }

        return $ipaddr;
    }

    /**
    * Return city name
    */
    public function getCityName()
    {
        if( $this->response && !empty($this->response->city) ) {
            return !empty($this->response->city->name_ru) ? $this->response->city->name_ru : null;
        }

        return null;
    }
}

$check_ip = new CheckIP();

if( ($city = $check_ip->getCityName()) ) {
    echo $city;

} else {
    echo 'Не удалось определить город...';
}

